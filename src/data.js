let shopItemsData = [{
    id:"a",
    name:"Mens Shirt",
    price: 10,
    desc: "Mens white shirt",
    img: "img/mens6.webp"
},
{
    id:"b",
    name:"Mens Shirt",
    price: 10,
    desc: "Mens blue shirt",
    img: "img/mens5.webp"
},
{
    id:"c",
    name:"Mens Jumper",
    price: 20,
    desc: "Mens Black Jumper",
    img: "img/mens3.webp"
},
{
    id:"d",
    name:"Mens Coat",
    price: 30,
    desc: "Mens Black Coat",
    img: "img/mens1.webp"
},
{
    id:"e",
    name:"Mens Hat",
    price: 5,
    desc: "Mens Black Wooly Hat",
    img: "img/mens2.webp"
},
{
    id:"g",
    name:"Mens Jeans",
    price: 15,
    desc: "Mens Blue Straight Jeans",
    img: "img/mens4.webp"
},
{
    id:"h",
    name:"Womens Shirt",
    price: 10,
    desc: "Womens White Shirt",
    img: "img/womensShirt1.webp"
},
{
    id:"i",
    name:"Womens Shirt",
    price: 10,
    desc: "Womens Black Shirt",
    img: "img/womensShirt2.webp"
},
{
    id:"j",
    name:"Womens Jumper",
    price: 15,
    desc: "Womens Pink Jumper",
    img: "img/womensJumper.webp"
},
{
    id:"k",
    name:"Womens Coat",
    price: 30,
    desc: "Womans Black Coat",
    img: "img/womenCoat.webp"
},
{
    id:"l",
    name:"Womans Hat",
    price: 5,
    desc: "Womans Black Bobble Hat",
    img: "img/womensHat.webp"
},
{
    id:"m",
    name:"Womans Jeans",
    price: 15,
    desc: "Womans Black Straight Jeans",
    img: "img/womensJeans.webp"
}];

let menItemsData = [{
    id:"a",
    name:"Mens Shirt",
    price: 10,
    desc: "Mens white shirt",
    img: "img/mens6.webp"
},
{
    id:"b",
    name:"Mens Shirt",
    price: 10,
    desc: "Mens blue shirt",
    img: "img/mens5.webp"
},
{
    id:"c",
    name:"Mens Jumper",
    price: 20,
    desc: "Mens Black Jumper",
    img: "img/mens3.webp"
},
{
    id:"d",
    name:"Mens Coat",
    price: 30,
    desc: "Mens Black Coat",
    img: "img/mens1.webp"
},
{
    id:"e",
    name:"Mens Hat",
    price: 5,
    desc: "Mens Black Wooly Hat",
    img: "img/mens2.webp"
},
{
    id:"g",
    name:"Mens Jeans",
    price: 15,
    desc: "Mens Blue Straight Jeans",
    img: "img/mens4.webp"
}];

let womenItemData = [{
    id:"h",
    name:"Womens Shirt",
    price: 10,
    desc: "Womens White Shirt",
    img: "img/womensShirt1.webp"
},
{
    id:"i",
    name:"Womens Shirt",
    price: 10,
    desc: "Womens Black Shirt",
    img: "img/womensShirt2.webp"
},
{
    id:"j",
    name:"Womens Jumper",
    price: 15,
    desc: "Womens Pink Jumper",
    img: "img/womensJumper.webp"
},
{
    id:"k",
    name:"Womens Coat",
    price: 30,
    desc: "Womans Black Coat",
    img: "img/womenCoat.webp"
},
{
    id:"l",
    name:"Womans Hat",
    price: 5,
    desc: "Womans Black Bobble Hat",
    img: "img/womensHat.webp"
},
{
    id:"m",
    name:"Womans Jeans",
    price: 15,
    desc: "Womans Black Straight Jeans",
    img: "img/womensJeans.webp"
}];