let label = document.getElementById("label");
let shoppingCart = document.getElementById("shopping-cart")

let basket = JSON.parse(localStorage.getItem("data")) || [];

let calculation = ()=>{
    let cartIcon = document.getElementById("cartAmount");
    cartIcon.innerHTML = basket.map((x)=> x.item).reduce((x,y)=>x+y,0);
};

calculation();

let generateCartItems = ()=>{
    if(basket.length !==0){
        return shoppingCart.innerHTML = basket.map((x)=>{
            let {id,item} = x;
            let search = shopItemsData.find((y)=>y.id === id) || [];
            return `
            <div class="cart-item">
                <img width="120" src=${search.img} alt="" />
                <div class="details">
                    <div class="title-price-x">
                        <h4 class="title-price">
                            <p>${search.name}</p>
                            <p class="cart-item-price">£${search.price}</p>
                        </h4>
                        <i onclick="removeItem(${id})" class="bi bi-x-lg"></i>
                    </div>

                    <div class="buttons">
                        <div id=${id} class="quantity">${item}</div>
                        <div class="bag">
                            <i onclick="removeCart(${id})" class="bi bi-bag-dash"></i>
                        </div>
                </div>

                    <h3>£${item * search.price}</h3>
                </div>
            </div>`
        }).join("")
    }else{
        shoppingCart.innerHTML = ``
        label.innerHTML = `
        <h2>Cart is Empty</h2> 
        <a class="catagory-center" href="Products.html">Back to Products</a>`
    }
};

let removeCart = (id)=>{
    let selectedItem = id;
    let search = basket.find((x)=>x.id === selectedItem.id);

    if(search === undefined) return;
    else if(search.item === 0) return;
    else (search.item -= 1);

    update(selectedItem.id);
    basket = basket.filter((x)=>x.item !==0);
    generateCartItems();
    localStorage.setItem("data", JSON.stringify(basket));
};

let update = (id)=>{
    let search = basket.find((x)=>x.id === id);
    document.getElementById(id).innerHTML = search.item;
    calculation();
    totalPrice();
};

let removeItem = (id)=>{
    let selectedItem = id;
    basket = basket.filter((x)=>x.id !== selectedItem.id);
    generateCartItems();
    totalPrice();
    localStorage.setItem("data", JSON.stringify(basket)); 
};

let totalPrice = ()=>{
    if(basket.length !== 0){
        let amount = basket.map((x)=>{
            let {item,id} =x;
            let search = shopItemsData.find((y)=>y.id === id) || [];
            return item * search.price;
        }).reduce((x,y)=>x+y,0);
    label.innerHTML = `
    <h2>Total: £${amount}</h2>
    <a onclick="clearCart()" href="Products.html" class="checkout">Checkout</a>
    <i onclick="clearCart()" class="Clear">Clear</i>
    `;
    }
    else return;
};

let clearCart = ()=>{
    basket =[];
    generateCartItems();
    localStorage.setItem("data", JSON.stringify(basket)); 
}

totalPrice();

generateCartItems();