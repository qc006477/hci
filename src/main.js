let shop = document.getElementById('shop');

console.log(shop);



let basket = JSON.parse(localStorage.getItem("data")) || [];

let generateMens =()=>{
    return (shop.innerHTML = menItemsData.map((x)=>{
        let{id,name,price,desc,img} = x;
        let search = basket.find((x)=>x.id === id) || [];
        return `
        <div id=product-id-${id} class ="item">
            <img width="221" src="${img}" alt="">
            <div class="details">
                <h3>${name}</h3>
                <p>${desc}</p>
                <div class="price-quantity">
                    <h2>£${price}</h2>
                    <div class="buttons">
                        <div id=${id} class="quantity">
                        ${search.item === undefined? 0: search.item}
                        </div>
                        <div class="bag">
                            <i onclick="addCart(${id})" class="bi bi-bag-plus"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     `;
    }).join(""));
};

let generateWomens =()=>{
    return (shop.innerHTML = womenItemData.map((x)=>{
        let{id,name,price,desc,img} = x;
        let search = basket.find((x)=>x.id === id) || [];
        return `
        <div id=product-id-${id} class ="item">
            <img width="221" src="${img}" alt="">
            <div class="details">
                <h3>${name}</h3>
                <p>${desc}</p>
                <div class="price-quantity">
                    <h2>£${price}</h2>
                    <div class="buttons">
                        <div id=${id} class="quantity">
                        ${search.item === undefined? 0: search.item}
                        </div>
                        <div class="bag">
                            <i onclick="addCart(${id})" class="bi bi-bag-plus"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     `;
    }).join(""));
};

let generateShop =()=>{
    return (shop.innerHTML = shopItemsData.map((x)=>{
        let{id,name,price,desc,img} = x;
        let search = basket.find((x)=>x.id === id) || [];
        return `
        <div id=product-id-${id} class ="item">
            <img width="221" src="${img}" alt="">
            <div class="details">
                <h3>${name}</h3>
                <p>${desc}</p>
                <div class="price-quantity">
                    <h2>£${price}</h2>
                    <div class="buttons">
                        <div id=${id} class="quantity">
                        ${search.item === undefined? 0: search.item}
                        </div>
                        <div class="bag">
                            <i onclick="addCart(${id})" class="bi bi-bag-plus"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     `;
    }).join(""));
};

generateShop();

let addCart = (id)=>{
    let selectedItem = id;
    let search = basket.find((x)=>x.id === selectedItem.id);

    if(search === undefined){
        basket.push({
            id: selectedItem.id,
            item: 1,
        });
    }
    else{
        search.item += 1;
    }

    localStorage.setItem("data", JSON.stringify(basket));

    update(selectedItem.id);
};

let update = (id)=>{
    let search = basket.find((x)=>x.id === id);
    document.getElementById(id).innerHTML = search.item;
    calculation();
};

let calculation = ()=>{
    let cartIcon = document.getElementById("cartAmount");
    cartIcon.innerHTML = basket.map((x)=> x.item).reduce((x,y)=>x+y,0);
};

calculation();